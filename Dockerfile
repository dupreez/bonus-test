FROM node
WORKDIR /app
RUN chown -R node:node /app
USER node

COPY package.json package-lock.json ./
RUN npm ci --ignore-scripts

COPY . .
RUN npm run build

CMD ["node", "dist/server.js"]
