import requestPromise from 'request-promise';

interface TokenResponseBody {
  access_token: string;
  scope: string;
  expires_in: number;
  token_type: string;
}

interface FileUploadResponseBody {
  id: string;
  uploadedAt: string;
  status: string;
}

interface ResultResponseBody {
  fileId: string;
  status: string;
  results: {
    timestamp: string;
    fields: Array<{
      name: string;
      value: any;
      confidence: number;
    }>,
  };
}

interface SyphtError {
  code: string;
  message: string;
}

interface File {
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  size: number;
  buffer: Buffer;
}

export class Sypht {
  static access_token: string;
  static expires_in: Date;
  private readonly apiUrl: string;
  private readonly authUrl: string;

  constructor(
    apiUrl: string,
    authUrl: string,
    private id: string,
    private secret: string,
  ) {
    this.apiUrl = apiUrl.replace(/\/$/, '');
    this.authUrl = authUrl.replace(/\/$/, '');
  }

  private async getAuth(): Promise<string> {
    const now = new Date();

    if (Sypht.access_token && Sypht.expires_in > now) {
      return Sypht.access_token;
    }

    return requestPromise
      .post({
        url: this.authUrl + '/oauth/token',
        json: true,
        body: {
          client_id: this.id,
          client_secret: this.secret,
          audience: this.apiUrl,
          grant_type: 'client_credentials',
        },
      })
      .then(({ expires_in, access_token }: TokenResponseBody) => {
        now.setSeconds(now.getSeconds() + expires_in);
        Sypht.expires_in = now;
        Sypht.access_token = access_token;

        return Sypht.access_token;
      });
  }

  uploadDocument(file: File): Promise<FileUploadResponseBody | SyphtError> {
    return this.getAuth()
      .then((bearer: string) =>
        requestPromise.post({
          url: this.apiUrl + '/fileupload',
          formData: {
            fileToUpload: {
              value: file.buffer,
              options: {
                filename: file.originalname,
                contentType: file.mimetype,
              },
            },
          },
          json: true,
          headers: {
            'Authorization': `Bearer ${bearer}`,
          },
        }).then((body: FileUploadResponseBody) => body)
          .catch(({ error }: { error: SyphtError }) => error));
  }

  fetchDocument(id: string): Promise<ResultResponseBody | SyphtError> {
    return this.getAuth()
      .then((bearer: string) =>
        requestPromise.get({
          url: this.apiUrl + '/result/final/' + id,
          json: true,
          headers: {
            'Authorization': `Bearer ${bearer}`,
          },
        }).then((body: ResultResponseBody) => body)
          .catch(({ error }: { error: SyphtError }) => error));
  }
}
