import express from 'express';
import multer from 'multer';
import { Sypht } from './Sypht';

const router = express();
const port = 3000;
const upload = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 20000000,
  }
});

const sypht = new Sypht(
  process.env.API_URL,
  process.env.API_AUTH,
  process.env.API_ID,
  process.env.API_SECRET
);

router.post('/document', upload.single('file'), (req, res) => {
  sypht.uploadDocument(req.file)
    .then((body) => res.json(body))
    .catch((error) => {
      console.error(error.stack);
      res.sendStatus(400);
    })
});

router.get('/document/:id', (req, res) => {
  sypht.fetchDocument(req.params.id)
    .then((body) => res.json(body))
    .catch((error) => {
      console.error(error.stack);
      res.sendStatus(400);
    })
});

router.listen(port, () => console.info('Listening on port %s...', port));
