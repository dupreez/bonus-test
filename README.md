# Bonus Solution
Node API.

## Usage
This project can be started locally by using the Node.js CLI or by creation of a docker container.

### Docker
Make a copy of `.env.example` to the root directory as `.env`, filling out API_ID and API_SECRET with Sypht credentials.

Build the docker container
```
docker build -t bonus-test .
```
Run using supplied docker-compose configuration, or through own exec
```
docker-compose up
```

### Node.js CLI
Install the node modules
```
npm i
```

Compile the scripts
```
npm run build
```

Run the server
```
API_URL=https://api.sypht.com \
API_AUTH=https://login.sypht.com \
API_ID=*** \
API_SECRET=*** \
node dist/server.js
```

## API
If running the application using the above process, the API should be accessible on http://localhost:3000. 

----

### POST /document
Upload a document to Sypht and return the id.

#### HTTP Request
```
Accepts: multipart/form-data
Content-Type: application/json
```
#### Request Body
| Parameters | Description   |
|:-----------|:-------------|
| file       | The file to upload, can be png, jpg or pdf.| 

----

### GET /document/:id
Fetch the results of the document

#### HTTP Request
```
Content-Type: application/json
```
